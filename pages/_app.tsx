import App, { AppContext } from 'next/app'
import Link from 'next/link'
import React from 'react'
import { ThemeProvider } from 'styled-components'
import EventContext from '../contexts/event.context'
import axios from 'axios'

const theme = {
  colors: {
    primary: '#0070f3'
  }
}
class MyApp extends App {
  static async getInitialProps({ Component, ctx }: AppContext) {
    let pageProps = {}
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }
    return { 
      pageProps
    }
  }
  render () {
    const { Component, pageProps } = this.props
    const { events } = pageProps
    return (
      <ThemeProvider theme={theme}>
            <EventContext.Provider value={events}>
              <Link href="/" as="/page"><a>page</a></Link> <br />
              <Link href="/" as="/page/child"><a>page child</a></Link> <br />
              <Link href="/" as="/otherpage"><a>other page</a></Link> <br />
              <Link href="/news/item" as="news/item"><a>news item</a></Link> <br />
              <Link href="/" as="/no-page-found"><a>no page found</a></Link>
              <Component {...pageProps} />
          </EventContext.Provider>
      </ThemeProvider>
    )
  }
}

export default MyApp