import React, { useContext } from 'react'
import Error from 'next/error'
import axios from 'axios'

const Index = ({ page, pathname, error }) => {
  if(error) return <Error statusCode={404} />
  console.log(page)
  return (
    <div>
      <p>Page {pathname}</p>
    </div>
  )
}

Index.getInitialProps = async ({ asPath }) => {
  console.log(asPath)
  if(asPath.includes('favicon.ico')) return {}
  try {
    const page = {}
    // api fetch and assign page
    return { pathname: asPath, page }
  } catch(e) {
    console.log(e)
    return { error: true }
  }
}

export default Index