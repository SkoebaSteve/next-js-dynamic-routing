import axios from 'axios'

const Index = ({ slug }) => (
  <div>
    <p>Page {slug}</p>
  </div>
)

Index.getInitialProps = async ({ query }) => {
  return { slug: query.slug }
}

export default Index